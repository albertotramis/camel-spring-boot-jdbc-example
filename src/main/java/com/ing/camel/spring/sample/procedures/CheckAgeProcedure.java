package com.ing.camel.spring.sample.procedures;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.StoredProcedure;

public class CheckAgeProcedure extends StoredProcedure{

	public CheckAgeProcedure(JdbcTemplate jdbcTemplate, String name) {
		super(jdbcTemplate, name);
		setFunction(false);
	}
}
