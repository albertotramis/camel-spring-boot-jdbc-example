package com.ing.camel.spring.sample.bean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ing.camel.spring.sample.dto.Person;
import com.ing.camel.spring.sample.model.PersonModel;
import com.ing.camel.spring.sample.repository.JdbcPersonRepository;

@Component("myBean")
public class MyBean {
    
	@Autowired
	private JdbcPersonRepository repository;
	
	public PersonModel instantiatePerson(Long id) throws FileNotFoundException, IOException {
    	PersonModel person = repository.findById(id);
    	return person;
    }
    
    public String isAdult(int age) throws Exception {
    	if (age >= 18 ) {
    		return " is adult" ;
    	} else {
    		throw new Exception("is not adult");
    	}
    }
    
    public int countPerson() throws FileNotFoundException, IOException {
    	int result = repository.count();
		return result;
    }
    
    public String throwException(Exception e) {
    	return e.getMessage();
    }

}
