package com.ing.camel.spring.sample.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.common.reflection.MetadataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.core.metadata.CallMetaDataProvider;

import com.ing.camel.spring.sample.model.PersonModel;
import com.ing.camel.spring.sample.procedures.CheckAgeProcedure;

public abstract class AbstractJdbcRepository {
	
	@Autowired
	private JdbcTemplate template;

	//private CallMetaDataProvider metaDataProvider;
	
	public int executeQueryForObjectInteger(String queryName) throws FileNotFoundException, IOException {
		File fileName = getFileFromResources(queryName);
		String query = getQueryFromFile(fileName);
		return template.queryForObject(query, Integer.class);
		
	}
	
	public int executeUpdate(String queryName, Object[] args) throws FileNotFoundException, IOException {
		File fileName = getFileFromResources(queryName);
		String query = getQueryFromFile(fileName);
		return template.update(query, args);
	}
	
	public List<PersonModel> getQueryToObject(String queryName, RowMapper<PersonModel> rowMapper) throws FileNotFoundException, IOException {
		File fileName = getFileFromResources(queryName);
		String query = getQueryFromFile(fileName);
		return template.query(query, rowMapper);
	}
	
	public PersonModel getQueryToObjectParams(String queryName, Object[] args, RowMapper<PersonModel> rowMapper) throws FileNotFoundException, IOException {
		File fileName = getFileFromResources(queryName);
		String query = getQueryFromFile(fileName);
		return template.queryForObject(query, args, rowMapper);
	}
	
	public String getSingleField(String queryName, Object[] args) throws FileNotFoundException, IOException {
		File fileName = getFileFromResources(queryName);
		String query = getQueryFromFile(fileName);
		return template.queryForObject(query, args, String.class);
	}
	
	public <T> List<T> callProcedure(String procedureName, SqlParameter[] paramArrayIn, SqlOutParameter paramOut, RowMapper<T> rowMapper, Object... params) {
		CheckAgeProcedure checkAgeProcedure= new CheckAgeProcedure(template, procedureName);
		SqlParameter[] fullList = new SqlParameter[paramArrayIn.length + 1];
		int i;
		for (i = 0; i < paramArrayIn.length; i++) {
			fullList[i] = paramArrayIn[i];
		}
		fullList[fullList.length - 1] = createReturnResultSetParameter(paramOut.getName(), rowMapper);
		checkAgeProcedure.setParameters(fullList);
		checkAgeProcedure.compile();
		
		Map storedProcResult = checkAgeProcedure.execute(params);
		return (List<T>) storedProcResult.get(paramOut.getName());
	}
	
	public Object callProcedure(String procedureName, SqlParameter[] paramArrayIn, SqlOutParameter paramOut, Object... params) {
		CheckAgeProcedure checkAgeProcedure= new CheckAgeProcedure(template, procedureName);
		SqlParameter[] fullList = new SqlParameter[paramArrayIn.length + 1];
		int i;
		for (i = 0; i < paramArrayIn.length; i++) {
			fullList[i] = paramArrayIn[i];
		}
		fullList[fullList.length - 1] = paramOut;
		checkAgeProcedure.setParameters(fullList);
		checkAgeProcedure.compile();
		
		Map storedProcResult = checkAgeProcedure.execute(params);
		return storedProcResult.get(paramOut.getName());
	}
	
	public void callProcedure(String procedureName, SqlParameter[] paramArrayIn, Object... params) {
		CheckAgeProcedure checkAgeProcedure= new CheckAgeProcedure(template, procedureName);
		checkAgeProcedure.setParameters(paramArrayIn);
		checkAgeProcedure.compile();
		
		checkAgeProcedure.execute(params);
	}
	
	
	private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource("sql/" + fileName + ".sql");
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
	
	private String getQueryFromFile(File file) throws FileNotFoundException, IOException {

		String fullQuery = "";
        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                fullQuery = fullQuery + line;
            }
        }
        return fullQuery;
	}
	
	private SqlParameter createReturnResultSetParameter(String parameterName, RowMapper<?> rowMapper) {
//		if (metaDataProvider.isReturnResultSetSupported()) {
			return new SqlReturnResultSet(parameterName, rowMapper);
//		}
//		else {
//			if (this.metaDataProvider.isRefCursorSupported()) {
//				return new SqlOutParameter(parameterName, this.metaDataProvider.getRefCursorSqlType(), rowMapper);
//			}
//			else {
//				throw new InvalidDataAccessApiUsageException("Return of a ResultSet from a stored procedure is not supported.");
//			}
//		}
	}

}
