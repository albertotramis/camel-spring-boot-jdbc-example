package com.ing.camel.spring.sample.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.ing.camel.spring.sample.model.PersonModel;

public class MyProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		PersonModel payload = exchange.getIn().getBody(PersonModel.class);
		exchange.getIn().setBody(payload.getAge());

	}

}
