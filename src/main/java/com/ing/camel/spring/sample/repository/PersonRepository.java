package com.ing.camel.spring.sample.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.ing.camel.spring.sample.model.PersonModel;



public interface PersonRepository {

	int count() throws FileNotFoundException, IOException;

    int save(PersonModel person) throws FileNotFoundException, IOException;

    int update(PersonModel person) throws FileNotFoundException, IOException;

    int deleteById(Long id) throws FileNotFoundException, IOException;

    List<PersonModel> findAll() throws FileNotFoundException, IOException;

    PersonModel findByNameAndSurname(String name, String surname) throws FileNotFoundException, IOException;

    PersonModel findById(Long id) throws FileNotFoundException, IOException;

    String getNameById(Long id) throws FileNotFoundException, IOException;

    List<PersonModel> getAdultness(String name, String surname);
	
	void savePerson(String name, String surname, int age);
}