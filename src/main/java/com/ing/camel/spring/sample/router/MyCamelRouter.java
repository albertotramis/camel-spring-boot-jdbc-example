package com.ing.camel.spring.sample.router;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ing.camel.spring.sample.bean.MyBean;
import com.ing.camel.spring.sample.processor.MyProcessor;

@Component
public class MyCamelRouter extends RouteBuilder {

	@Autowired
	MyBean myBean;

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).to("direct:exception");
		
		from("direct:multicastRoute1").bean(myBean, "instantiatePerson").process(new MyProcessor()).to("direct:multicastRoute2");

		from("direct:multicastRoute2").bean(myBean, "isAdult");
		
		from("direct:multicastRoute3").bean(myBean, "countPerson");
		
		from("direct:exception").bean(myBean, "throwException");

	}
}