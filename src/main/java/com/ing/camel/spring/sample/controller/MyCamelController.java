package com.ing.camel.spring.sample.controller;

import org.apache.camel.EndpointInject;
import org.apache.camel.FluentProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ing.camel.spring.sample.repository.JdbcPersonRepository;
import com.ing.camel.spring.sample.repository.PersonRepository;

@RestController
public class MyCamelController {

	@EndpointInject(value = "direct:multicastRoute1")
    private FluentProducerTemplate producer;
	
	@EndpointInject(value = "direct:multicastRoute3")
	private FluentProducerTemplate count;
	
	@Autowired
	private JdbcPersonRepository repository;
	
	@RequestMapping(value = "/getAllPeople/{id}", method = RequestMethod.GET)
	public String getAllPeople(@PathVariable(name = "id") Long id) {
		producer.withBody(id);
		return producer.request(String.class);
	}
	
	@RequestMapping(value = "/countAllPeople", method = RequestMethod.GET)
	public Integer countAllPeople() {
		return count.request(int.class);
	}
	
	@RequestMapping(value = "/getProcedures/{name}/{surname}", method = RequestMethod.GET)
	public String getProcedures(@PathVariable(name = "name") String name,
								@PathVariable(name = "surname") String surname) {
		return repository.getAdultness(name, surname).toString();
	}
	
	@RequestMapping(value = "/setPerson/{name}/{surname}/{age}", method = RequestMethod.GET)
	public void setPerson(@PathVariable(name = "name") String name,
						  @PathVariable(name = "surname") String surname,
						  @PathVariable(name = "age") int age) {
		repository.savePerson(name, surname, age);
	}
}
