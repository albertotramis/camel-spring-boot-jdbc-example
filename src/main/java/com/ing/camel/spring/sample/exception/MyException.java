package com.ing.camel.spring.sample.exception;

public class MyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String errore;
	
	public MyException() {
		super();
		errore = "Is not adult";
		System.out.println(errore);
	}
	
	public String getErrore () {
		return errore;
	}

}
