package com.ing.camel.spring.sample.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import com.ing.camel.spring.sample.model.PersonModel;

@Repository
public class JdbcPersonRepository extends AbstractJdbcRepository implements PersonRepository{
	
	@Override
	public int count() throws FileNotFoundException, IOException {
		return executeQueryForObjectInteger("count");
	}

	@Override
	public int save(PersonModel person) throws FileNotFoundException, IOException {
		return executeUpdate("save", new Object[] {person.getName(), person.getSurname(), person.getAge()});
	}

	@Override
	public int update(PersonModel person) throws FileNotFoundException, IOException {
		return executeUpdate("update", new Object[] {person.getName(), person.getPersonId()});
	}

	@Override
	public int deleteById(Long id) throws FileNotFoundException, IOException {
		return executeUpdate("delete", new Object[] {id});
	}

	@Override
	public List<PersonModel> findAll() throws FileNotFoundException, IOException {
		return getQueryToObject("findAll", createRowMapper());
	}

	@Override
	public PersonModel findByNameAndSurname(String name, String surname) throws FileNotFoundException, IOException {
		return getQueryToObjectParams("findByNameAndSurname", new Object[] {"%" + name + "%", "%" + surname + "%"}, createRowMapper());
		
	}

	@Override
	public PersonModel findById(Long id) throws FileNotFoundException, IOException {
		return getQueryToObjectParams("findById", new Object[] {id}, createRowMapper());
	}

	@Override
	public String getNameById(Long id) throws FileNotFoundException, IOException {
		return getSingleField("getNameById", new Object[] {id});
	}
	
	@Override
	public List<PersonModel> getAdultness(String name, String surname) {
		SqlParameter[] parametersIn = new SqlParameter[] {new SqlParameter("nameP", Types.VARCHAR), new SqlParameter("surnameP", Types.VARCHAR)};
		SqlOutParameter outParam = new SqlOutParameter("personP", Types.REF_CURSOR	);
		return callProcedure("GetPersonByNameAndSurname", parametersIn, outParam, createRowMapper(), name, surname);
	}
	
	@Override
	public void savePerson(String name, String surname, int age) {
		SqlParameter[] parametersIn = new SqlParameter[] {new SqlParameter("nameP", Types.VARCHAR), new SqlParameter("surnameP", Types.VARCHAR), new SqlParameter("ageP", Types.BIGINT)};
		callProcedure("AddPerson", parametersIn, name, surname, age);
	}
	
	private RowMapper<PersonModel> createRowMapper() {
		return (rs, rowNum) ->
        new PersonModel(
                rs.getLong("person_id"),
                rs.getString("name"),
                rs.getString("surname"),
                rs.getInt("age")
        );
	}

}