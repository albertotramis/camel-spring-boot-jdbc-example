package sample.camel;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ing.camel.spring.sample.bean.MyBean;
import com.ing.camel.spring.sample.model.PersonModel;

@SpringBootTest
public class MyBeanTest {
	
	@Autowired
	private MyBean myBean;
	
	@Test
	public void instantiatePersonTest() throws FileNotFoundException, IOException {
		assertEquals(myBean.instantiatePerson(1l), new PersonModel(1l, "Alberto", "Tramis", 21));
	}
	
	@Test
	public void isAdultTest() throws Exception {
		assertEquals(myBean.isAdult(18), " is adult" );
	}
	
	@Test
	public void isNotAdultTest() throws Exception {
		assertEquals(myBean.isAdult(17), " is not adult" );
	}
	
	@Test
	public void countTest() throws FileNotFoundException, IOException {
		assertEquals(myBean.countPerson(), 5);
	}
}
