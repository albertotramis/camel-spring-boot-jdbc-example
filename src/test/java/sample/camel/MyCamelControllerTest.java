package sample.camel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ing.camel.spring.sample.controller.MyCamelController;

@SpringBootTest
public class MyCamelControllerTest {
	
	@Autowired
	private MyCamelController controller;
	
	@Test
	public void getAllpeopleTestAdult() {
		assertEquals(controller.getAllPeople(1L), " is adult ");
	};
	
	@Test
	public void getAllpeopleTestNotAdult() {
		assertEquals(controller.getAllPeople(3L), " is not adult ");
	};
	
	@Test
	public void countAllPeopleTest() {
		assertEquals(controller.countAllPeople(), new Integer(5));
	};
}
